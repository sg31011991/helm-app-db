const express = require('express')
const morgan = require('morgan')
const createError = require('http-errors')
require('dotenv').config()
require('./helpers/init_mongodb')
const { verifyAccessToken } = require('./helpers/jwt_helper')
const AuthRoute = require('./Routes/Auth.route')


const app = express()
app.use(morgan('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(express.static('public'))
app.get('/', async(req, res, next) => {
    res.send('Hello from express.')
})

app.get('/protected', verifyAccessToken, async(req, res, next) => {
    console.log(req.headers['authorization'])
    res.send('This is protected Route.')
})

// ###########################
app.get('/api', async(req, res, next) => {
    const jwt = require('jsonwebtoken');
    const fs = require('fs');
    // const expressjwt = require('express-jwt')
    const jwksClient = require('jwks-rsa');
    const { expressjwt: jwt1 } = require("express-jwt");


    const privateKey = fs.readFileSync("./certs/private.pem");
    const token = jwt.sign({}, privateKey, { expiresIn: '1d', algorithm: 'RS256' });
    res.send({ token })
    app.use(
        jwt1({
            secret: jwksClient.expressJwtSecret({
                jwksUri: "http://localhost:8080/.well-known/jwks.json",
                cache: true,
                rateLimit: true
            }),
            algorithms: ["RS256"],
        }).unless({ path: ["/"] })
    );


})
app.get("/apiprotected", async(req, res, next) => {
    // const getregisters = await User.find({});
    // res.send(getregisters);
    res.send({ message: 'This is API protected route' });

})

// #####################
app.use('/auth', AuthRoute)
app.use(async(req, res, next) => {
    next(createError.NotFound('This Route doesnot exists'))
})

app.use((err, req, res, next) => {
    res.status(err.status || 500)
    res.send({
        error: {
            status: err.status || 500,
            message: err.message,
        },
    })
})



const PORT = process.env.PORT || 8080

app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`)
})