const fs = require('fs');
const rsaPemToJwk = require('rsa-pem-to-jwk');
const privatekey = fs.readFileSync('private.pem');
const jwk = rsaPemToJwk(privatekey, { use: 'sig' }, 'public');
console.log(jwk)